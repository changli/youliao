import com.seahorse.youliao.YouliaoWebApplication;
import com.seahorse.youliao.service.entity.FmsPayOrderDTO;
import com.seahorse.youliao.service.queue.RedisDelayedQueue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.api.RBlockingQueue;
import org.redisson.api.RDelayedQueue;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @ProjectName: jinkai-parent
 * @Package: PACKAGE_NAME
 * @ClassName: ApplicationTest
 * @Description: TODO
 * @author:songqiang
 * @Date:2020-07-01 16:21
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = YouliaoWebApplication.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApplicationTest {

    @Autowired
    private RedisDelayedQueue queue;

    @Autowired
    RedissonClient redissonClient;



    @Test
    public void test(){

        FmsPayOrderDTO dto = new FmsPayOrderDTO();
        dto.setCreateTime(new Date());
        dto.setOrderNo("123");
        queue.addQueueData(dto,1,TimeUnit.MINUTES);

        System.out.println("加入队列"+new Date());

        RBlockingQueue<FmsPayOrderDTO> blockingFairQueue = redissonClient.getBlockingQueue("order");
        RDelayedQueue<FmsPayOrderDTO> delayedQueue = redissonClient.getDelayedQueue(blockingFairQueue);
        FmsPayOrderDTO orderDTO = delayedQueue.poll();
        System.out.println("获取");
        if(orderDTO != null){
            System.out.println("取出队列"+orderDTO.getCreateTime());
        }
    }
}
